#!/usr/bin/python3
import os
import sqlite3
from Tkinter import *
import ttk

import main


###Functions

#Window commands
def exitProg():
  root.destroy()
def quit(event):
  root.destroy()

###Initial window creation

root = Tk()
root.title("Weather")
w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (w, h))


###Menu bar


##Menu functions

#Actions menu
def exportFile():
  return

def exportPdf():
  return

def exportWeb():
  return

#Actions menu
def updateData():
  return

def clearGraph():
  return

def clearAllData():
  return


##Menu bar creation

root.option_add('*tearOff', FALSE)

menubar = Menu(root)
root['menu'] = menubar

menu_file = Menu(menubar)
menu_actions = Menu(menubar)

menubar.add_cascade(menu=menu_file, label='File')
menubar.add_cascade(menu=menu_actions, label='Actions')

menu_file.add_command(label='Export as File', command=exportFile)
menu_file.add_command(label='Export as PDF', command=exportPdf)
menu_file.add_command(label='Export as Webpage', command=exportWeb)
menu_file.add_separator()
menu_file.add_command(label='Exit', command=exitProg)

menu_actions.add_command(label='Update Data', command=updateData)
menu_actions.add_command(label='Clear Graph', command=clearGraph)
menu_actions.add_command(label='Clear All Data', command=clearAllData)

#########################






mainframe = ttk.Frame(root)
mainframe.grid(column=0, row=0)

GenFrame = ttk.Frame(mainframe)
GenFrame.grid(column=0, row=0)
GenFrame['borderwidth'] = 2
GenFrame['relief'] = 'sunken'

StationFrame = ttk.Frame(mainframe)
StationFrame.grid(column=1, row=0)
StationFrame['borderwidth'] = 2
StationFrame['relief'] = 'sunken'

GraphFrame = ttk.Frame(mainframe)
GraphFrame.grid(column=0, row=1, columnspan=2)
GraphFrame['borderwidth'] = 2
GraphFrame['relief'] = 'sunken'

##Fill frames

#GenFrame
create = ttk.Radiobutton(GenFrame, text='Create New', variable=GenFrame, value='create', command=clearAllData).grid(column=0, row=0)
edit = ttk.Radiobutton(GenFrame, text='Edit Existing', variable=GenFrame, value='edit', command=clearAllData).grid(column=0, row=1)
delete = ttk.Radiobutton(GenFrame, text='Delete', variable=GenFrame, value='delete', command=clearAllData).grid(column=0, row=2)

#StationFrame
create = ttk.Radiobutton(StationFrame, text='Create New', variable=StationFrame, value='create', command=clearAllData).grid(column=0, row=0)
edit = ttk.Radiobutton(StationFrame, text='Edit Existing', variable=StationFrame, value='edit', command=clearAllData).grid(column=0, row=1)
delete = ttk.Radiobutton(StationFrame, text='Delete', variable=StationFrame, value='delete', command=clearAllData).grid(column=0, row=2)

#GraphFrame
create = ttk.Radiobutton(GraphFrame, text='Create New', variable=GraphFrame, value='create', command=clearAllData).grid(column=0, row=0)
edit = ttk.Radiobutton(GraphFrame, text='Edit Existing', variable=GraphFrame, value='edit', command=clearAllData).grid(column=0, row=1)
delete = ttk.Radiobutton(GraphFrame, text='Delete', variable=GraphFrame, value='delete', command=clearAllData).grid(column=0, row=2)




###Final matters

root.bind('<Control-q>', quit)
root.mainloop()
