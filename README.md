#Historic Data Viewing Tool

Originally a very old scripted program for producing historic weather gifs, I now intend to create a tool to simply plot historic weather data as obtained from the MetOffice

##General Overview

This tool downloads weather data from the MetOffice website for weather stations across the United Kingdom, and extracts plottable data. It will plot data across the categories of Rainfall, DayLight Hours, and Temperature maximums and minimums. The data files have month increments so it makes sense to produce yearly averages, month per years, and month and year both for each station and averaged over the UK. This should help spot trends  and weather pattern.

##Structure

This will be a Python run system with a Tkinter frontend and an SQLite backend. See "toDo.txt" for more details.
